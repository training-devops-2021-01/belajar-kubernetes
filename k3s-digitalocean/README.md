# Deploy K3S di Digital Ocean #

* Setup Digital Ocean CLI (doctl)
* Install k3s
* Deploy aplikasi

## Setup doctl ##

* Ikuti petunjuk instalasi di [website resminya](https://github.com/digitalocean/doctl)

* Instalasi di MacOS

    ```
    brew install doctl
    ```

* Generate API Key di web interface DO. Klik menu API di kiri bawah 

    * Klik generate new token 

    [![API Key](./img/do-new-token.png)](./img/do-new-token.png))

    * Copy nilai token yang ditampilkan

    [![API Token](./img/do-api-token.png)](./img/do-api-token.png)

* Setup dengan perintah `doctl auth init`

* Melihat daftar SSH Key yang sudah terdaftar

    ```
    doctl compute ssh-key list
    ```

* Melihat pilihan ukuran droplet

    ```
    doctl compute size list
    ```

* Melihat pilihan region

    ```
    doctl compute region list 
    ```

* Melihat pilihan distro

    ```
    doctl compute image list-distribution
    ```

* Membuat droplet Ubuntu 20.04 seharga $5 di region Singapura

    ```
    doctl compute droplet \
          create k3s-1 \
          --size s-1vcpu-1gb \
          --image ubuntu-20-04-x64 \
          --region sgp1 \
          --tag-name belajar-k3s \
          --ssh-keys 714736
    ```

* Lihat daftar droplet

    ```
    doctl compute droplet list --format "ID,Name,PublicIPv4"
    ```

## Membuat cluster kubernetes dengan k3sup ##

* Instalasi `k3sup` di MacOS

    ```
    brew install k3sup
    ```

* Export alamat IP server kita di environment variable

   ```
   export K3S_SERVER_IP=206.189.34.185
   ```

* Install k3s node (master + worker)

    ```
    k3sup install --ip $K3S_SERVER_IP --user root --local-path=./kubeconfig
    ```

    Outputnya sebagai berikut:

    ```
    Running: k3sup install
    2021/03/21 11:18:15 206.189.34.185
    Public IP: 206.189.34.185
    [INFO]  Finding release for channel v1.19
    [INFO]  Using v1.19.8+k3s1 as release
    [INFO]  Downloading hash https://github.com/k3s-io/k3s/releases/download/v1.19.8+k3s1/sha256sum-amd64.txt
    [INFO]  Downloading binary https://github.com/k3s-io/k3s/releases/download/v1.19.8+k3s1/k3s
    [INFO]  Verifying binary download
    [INFO]  Installing k3s to /usr/local/bin/k3s
    [INFO]  Creating /usr/local/bin/kubectl symlink to k3s
    [INFO]  Creating /usr/local/bin/crictl symlink to k3s
    [INFO]  Creating /usr/local/bin/ctr symlink to k3s
    [INFO]  Creating killall script /usr/local/bin/k3s-killall.sh
    [INFO]  Creating uninstall script /usr/local/bin/k3s-uninstall.sh
    [INFO]  env: Creating environment file /etc/systemd/system/k3s.service.env
    Created symlink /etc/systemd/system/multi-user.target.wants/k3s.service → /etc/systemd/system/k3s.service.
    [INFO]  systemd: Creating service file /etc/systemd/system/k3s.service
    [INFO]  systemd: Enabling k3s unit
    [INFO]  systemd: Starting k3s
    Result: [INFO]  Finding release for channel v1.19
    [INFO]  Using v1.19.8+k3s1 as release
    [INFO]  Downloading hash https://github.com/k3s-io/k3s/releases/download/v1.19.8+k3s1/sha256sum-amd64.txt
    [INFO]  Downloading binary https://github.com/k3s-io/k3s/releases/download/v1.19.8+k3s1/k3s
    [INFO]  Verifying binary download
    [INFO]  Installing k3s to /usr/local/bin/k3s
    [INFO]  Creating /usr/local/bin/kubectl symlink to k3s
    [INFO]  Creating /usr/local/bin/crictl symlink to k3s
    [INFO]  Creating /usr/local/bin/ctr symlink to k3s
    [INFO]  Creating killall script /usr/local/bin/k3s-killall.sh
    [INFO]  Creating uninstall script /usr/local/bin/k3s-uninstall.sh
    [INFO]  env: Creating environment file /etc/systemd/system/k3s.service.env
    [INFO]  systemd: Creating service file /etc/systemd/system/k3s.service
    [INFO]  systemd: Enabling k3s unit
    [INFO]  systemd: Starting k3s
    Created symlink /etc/systemd/system/multi-user.target.wants/k3s.service → /etc/systemd/system/k3s.service.

    Saving file to: /Users/endymuhardin/workspace/training/training-devops-2021-01/belajar-kubernetes/k3s-digitalocean/kubeconfig

    # Test your cluster with:
    export KUBECONFIG=./kubeconfig
    kubectl config set-context default
    kubectl get node -o wide
    ```

* Export file konfigurasi yang dihasilkan menjadi environment variable

    ```
    export KUBECONFIG=/Users/endymuhardin/workspace/training/training-devops-2021-01/belajar-kubernetes/k3s-digitalocean/kubeconfig
    ```

* Test get node

    ```
    kubectl get node -o wide
    ```

    Outputnya seperti ini

    ```
    NAME    STATUS   ROLES    AGE   VERSION        INTERNAL-IP      EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION     CONTAINER-RUNTIME
    k3s-1   Ready    master   4m    v1.19.8+k3s1   206.189.34.185   <none>        Ubuntu 20.04.1 LTS   5.4.0-51-generic   containerd://1.4.3-k3s3
    ```

* Setup digitalocean API key dalam Kubernetes Secret

    ```
    kubectl create -f 20-do-access-token-yml
    ```

* Enable CSI plugin untuk persistent volume

    ```
    kubectl apply -fhttps://raw.githubusercontent.com/digitalocean/csi-digitalocean/master/deploy/kubernetes/releases/csi-digitalocean-v2.1.1/{crds.yaml,driver.yaml,snapshot-controller.yaml}
    ```

* Membuat persistent volume dengan Digital Ocean Block Storage

    ```
    kubectl apply -f 21-persistent-volume-do.yml
    ```

* Cek kondisi PVC

    ```
    kubectl get pvc
    ```

    Seharusnya statusnya `Bound`

    ```
    NAME             STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS       AGE
    pvc-db-antrian   Bound    pvc-86bd6bf7-5af3-406d-a50e-cc1ddba9859d   1Gi        RWO            do-block-storage   105m
    ```

* Deploy pod database

    ```
    kubectl apply -f ../k8s-deployments/30-database-mariadb.yml
    ```

* Cek status

    ```
    kubectl get pvc,pods,services
    ```

* Deploy pod aplikasi

    ```
    kubectl apply -f ../k8s-deployments/40-aplikasi-expressjs.yml
    ```

* Cek alamat IP public aplikasi

    ```
    kubectl get services
    ```

    Outputnya seperti ini

    ```
    NAME                  TYPE           CLUSTER-IP     EXTERNAL-IP      PORT(S)           AGE
    kubernetes            ClusterIP      10.43.0.1      <none>           443/TCP           152m
    antrian-db-service    ClusterIP      10.43.30.178   <none>           3306/TCP          28m
    app-antrian-service   LoadBalancer   10.43.39.144   206.189.34.185   11111:32307/TCP   2m33s
    ```

* Browse ke [http://206.189.34.185:11111/antrian](http://206.189.34.185:11111/antrian). Sesuaikan IP dengan output `EXTERNAL-IP`

    Outputnya seperti ini:

    [![Error Skema DB](./img/error-skema-db.png)](./img/error-skema-db.png)

    Error karena skema database belum dibuat.

* Jalankan migrasi database

    ```
    kubectl apply -f https://gitlab.com/training-devops-2021-01/database-migration/-/raw/master/k8s-job-migration.yml
    ```

* Cek status migrasi database

    ```
    kubectl get jobs, pods
    ```

    Outputnya seperti ini kalau sudah oke

    ```
    NAME                                      COMPLETIONS   DURATION   AGE
    job.batch/database-migration-2021031801   1/1           26s        44s

    NAME                                              READY   STATUS      RESTARTS   AGE
    pod/antrian-db-dbbc8d67c-vzlsc                    1/1     Running     2          31m
    pod/svclb-app-antrian-service-kb7hg               1/1     Running     0          6m23s
    pod/aplikasi-antrian-expressjs-6449bfb69c-p9rlg   1/1     Running     0          6m22s
    pod/database-migration-2021031801-hqmfd           0/1     Completed   0          45s
    ```

* Test lagi, harusnya sudah oke

    [![Sudah oke](./img/data-antrian.png)](./img/data-antrian.png)

## Referensi ##

* [k3s Simple Setup](https://blog.alexellis.io/create-a-3-node-k3s-cluster-with-k3sup-digitalocean/)
* [k3s Production Setup](https://rancher.com/blog/2020/k3s-high-availability)
* [Ingress untuk k3s](https://blog.alexellis.io/ingress-for-your-local-kubernetes-cluster/)