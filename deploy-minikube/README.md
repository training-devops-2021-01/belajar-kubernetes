# Local Cluster dengan Minikube #

1. Menjalankan minikube

    ```
    minikube start
    ```

    outputnya seperti ini

    ```
    😄  minikube v1.18.1 on Darwin 11.2.3
    ✨  Automatically selected the docker driver. Other choices: hyperkit, virtualbox, ssh
    👍  Starting control plane node minikube in cluster minikube
    🚜  Pulling base image ...
    💾  Downloading Kubernetes v1.20.2 preload ...
        > preloaded-images-k8s-v9-v1....: 242.80 MiB / 491.22 MiB  49.43% 8.25 KiB ^C⏎                                                                                                            !  ~  minikube start                                                                                                                                       3.5m  Wed Mar 17 15:40:35 2021
    😄  minikube v1.18.1 on Darwin 11.2.3
    ✨  Automatically selected the docker driver. Other choices: hyperkit, virtualbox, ssh
    👍  Starting control plane node minikube in cluster minikube
    🚜  Pulling base image ...
    💾  Downloading Kubernetes v1.20.2 preload ...
        > preloaded-images-k8s-v9-v1....: 491.22 MiB / 491.22 MiB  100.00% 10.41 Mi
    🔥  Creating docker container (CPUs=2, Memory=1988MB) ...
    🐳  Preparing Kubernetes v1.20.2 on Docker 20.10.3 ...
        ▪ Generating certificates and keys ...
        ▪ Booting up control plane ...
        ▪ Configuring RBAC rules ...
    🔎  Verifying Kubernetes components...
        ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v4
    🌟  Enabled addons: default-storageclass, storage-provisioner
    🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
    ```

2. Test instalasi

    ```
    kubectl version
    ```

    outputnya seperti ini

    ```json
    Client Version: version.Info{Major:"1", Minor:"20+", GitVersion:"v1.20.4-dirty", GitCommit:"e87da0bd6e03ec3fea7933c4b5263d151aafd07c", GitTreeState:"dirty", BuildDate:"2021-03-15T09:58:13Z", GoVersion:"go1.16.2", Compiler:"gc", Platform:"darwin/amd64"}
    Server Version: version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.2", GitCommit:"faecb196815e248d3ecfb03c680a4507229c2a56", GitTreeState:"clean", BuildDate:"2021-01-13T13:20:00Z", GoVersion:"go1.15.5", Compiler:"gc", Platform:"linux/amd64"}
    ```

3. Deploy aplikasi

    ```
    kubectl apply -f deploy-minikube
    kubectl apply -f k8s-deployments
    ```

4. Jalankan database migration untuk membuat skema database dan sample data

    ```
    kubectl apply -f https://gitlab.com/training-devops-2021-01/database-migration/-/raw/master/k8s-job-migration.yml
    ```

5. Melihat object yang ada dalam cluster

    ```
    kubectl get pods,services,pvc,pv,deployments,jobs
    ```

    Outputnya seperti ini 

    ```
    NAME                             READY   STATUS              RESTARTS   AGE
    pod/antrian-db-dbbc8d67c-m5v9p   0/1     ContainerCreating   0          9s

    NAME                         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
    service/antrian-db-service   ClusterIP   10.110.255.170   <none>        3306/TCP   10s
    service/kubernetes           ClusterIP   10.96.0.1        <none>        443/TCP    50m

    NAME                                   STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
    persistentvolumeclaim/pvc-db-antrian   Bound    pvc-9a82f1f9-9c45-4f50-b583-771d18051792   1Gi        RWO            standard       10s

    NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM                    STORAGECLASS   REASON   AGE
    persistentvolume/pv-db                                      1Gi        RWO            Retain           Available                                                    113s
    persistentvolume/pvc-9a82f1f9-9c45-4f50-b583-771d18051792   1Gi        RWO            Delete           Bound       default/pvc-db-antrian   standard                9s

    NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/antrian-db   0/1     1            0           10s
    ```

    * Melihat kondisi semua pod

    ```
    kubectl describe pods
    ```

    * Melihat log pod 

    ```
    kubectl logs <nama pod>
    ```

6. Menjalankan `minikube tunnel` supaya LoadBalancer bisa mendapatkan External IP

    * Jalankan tunnel di terminal window baru

    ```
    minikube tunnel
    ```

7. Browse aplikasinya di [http://localhost:11111/antrian](http://localhost:11111/antrian)

8. Undeploy object di k8s

    * Undeploy satu file konfigurasi

    ```
    kubectl delete -f deploy-minikube/03-aplikasi-expressjs.yml
    ```

    * Undeploy seluruh folder

    ```
    kubectl delete -f deploy-minikube/
    ```

# Horizontal Pod Autoscale #

1. Deploy aplikasi ke k8s cluster

2. Deploy `metrics-server` ke k8s cluster

    ```
    minikube addons enable metrics-server
    ```

    outputnya seperti ini

    ```
    ▪ Using image k8s.gcr.io/metrics-server-amd64:v0.2.1
    🌟  The 'metrics-server' addon is enabled
    ```

3. Lihat nama deployment aplikasi web

    ```
    kubectl get deployments
    ```

4. Enable HPA untuk deployment `aplikasi-antrian-expressjs`

    ```
    kubectl autoscale deployment aplikasi-antrian-expressjs --cpu-percent=50 --min=1 --max=5
    ```

    outputnya seperti ini

    ```
    horizontalpodautoscaler.autoscaling/aplikasi-antrian-expressjs autoscaled
    ```

5. Lihat status HPA

    ```
    kubectl get hpa
    ```

6. Tambahkan load

    ```
    kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://app-antrian-service:11111/antrian; done"
    ```
   
    atau menggunakan aplikasi `ab`

    ```
    ab -c 5 -n 1000 -t 100000 http://127.0.0.1:11111/antrian
    ```

7. Cek jumlah replica

    ```
    kubectl get deployment aplikasi-antrian-expressjs
    ```

    dan kondisi masing-masing pod

    ```
    kubectl get pods
    ```
